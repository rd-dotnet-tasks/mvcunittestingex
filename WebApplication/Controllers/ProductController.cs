﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService ProductService;

        public ProductController(IProductService productService)
        {
            ProductService = productService;
        }

        public ActionResult Products()
        {
            ViewBag.Title = "Products";
            IEnumerable<ProductDetailDto> productList = ProductService.GetAllEffectiveProductDetails();
            ProductModels.ProductCategoryListModel model = new ProductModels.ProductCategoryListModel
            {
                ProductDetails = ProductService.GetAllEffectiveProductDetails(),

                ProductCategoryList = productList.Select(x => new SelectListItem
                {
                    Value = x.FKProductId.ToString(),
                    Text = x.Name
                })
            };
            return View(model);
        }
    }

    public class ProductDetailDto
    {
        public int FKProductId { get; set; }
        public string Name { get; set; }
    }

    public class ProductModels
    {
        public class ProductCategoryListModel
        {
            public IEnumerable<ProductDetailDto> ProductDetails { get; set; }
            public IEnumerable<SelectListItem> ProductCategoryList { get; set; }
        }
    }

    public interface IProductService
    {
        IEnumerable<ProductDetailDto> GetAllEffectiveProductDetails();
    }

    public class ProductService : IProductService
    {
        public IEnumerable<ProductDetailDto> GetAllEffectiveProductDetails()
        {
            throw new NotImplementedException();
        }
    }
}