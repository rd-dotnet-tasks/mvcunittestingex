﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplication;
using WebApplication.Controllers;
using Moq;

namespace WebApplication.Tests.Controllers
{
    [TestClass]
    public class ProductControllerTest
    {
        private Mock<IProductService> productServiceMock;

        private ProductController target;

        [TestInitialize]
        public void Init()
        {
            productServiceMock = new Mock<IProductService>();

            target = new ProductController(
                productServiceMock.Object);
        }

        [TestMethod]
        public void Products()
        {
            //arrange
            // There is a setup of 'GetAllEffectiveProductDetails'
            // When 'GetAllEffectiveProductDetails' method is invoked 'expectedallProducts' collection is exposed.
            var expectedallProducts = new List<ProductDetailDto> { new ProductDetailDto() };
            productServiceMock
                .Setup(it => it.GetAllEffectiveProductDetails())
                .Returns(expectedallProducts);

            //act
            var result = target.Products();

            //assert
            var model = (result as ViewResult).Model as ProductModels.ProductCategoryListModel;
            Assert.AreEqual(model.ProductDetails, expectedallProducts);
        }
    }
}
